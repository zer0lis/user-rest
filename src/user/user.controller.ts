import { Get, Post, Body, Put, Delete, Param, Controller, UsePipes, HttpException } from '@nestjs/common';
import { Request } from 'express';
import { UserService } from './user.service';
import { UserRO } from './user.interface';
import { CreateUserDto, UpdateUserDto, LoginUserDto } from './dto';
import { User } from './user.decorator';
import { ValidationPipe } from '../shared/pipes/validation.pipe';

import {
  ApiUseTags,
  ApiBearerAuth
} from '@nestjs/swagger';

@ApiBearerAuth()
@ApiUseTags('user')
@Controller()
export class UserController {

  constructor(private readonly userService: UserService) {}

  @Get('user')
  async findMe(@User('email') email: string): Promise<UserRO> {
    return await this.userService.findByEmail(email);
  }

  @Put('user')
  async update(@Body('user') userData: UpdateUserDto) {
    return await this.userService.update(userData);
  }

  @Get('users')
  async findAllUsers() {
    return await this.userService.findAll();
  }

  @UsePipes(new ValidationPipe())
  @Post('users')
  async create(@Body('user') userData: CreateUserDto) {
    return this.userService.create(userData);
  }

  @Delete('users/:slug')
  async delete(@Param() params) {
    return await this.userService.delete(params.slug);
  }

  @UsePipes(new ValidationPipe())
  @Post('users/login')
  async login(@Body('user') loginUserDto: LoginUserDto): Promise<UserRO> {
    const _user = await this.userService.findOne(loginUserDto);

    const errors = {User: ' not found'};
    if (!_user) throw new HttpException({errors}, 412);

    const dto: UpdateUserDto = {
      id: _user.id,
      loginCount: _user.loginCount + 1
    };
    const _updatedUser = await this.update(dto);
    if (!_updatedUser) throw new HttpException({User: 'could not update login count'}, 500);

    const token = await this.userService.generateJWT(_user);
    const roles = _user.roles.map( role => {
      return role.role;
    });
    let {id, email, bio, loginCount} = _user;
    const user = {id, email, token, bio, loginCount, roles};
    user.loginCount++;

    return {user}
  }
}