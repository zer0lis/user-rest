import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, getRepository } from 'typeorm';
import {RoleEntity, UserEntity} from './user.entity';
import {CreateUserDto, LoginUserDto, UpdateUserDto} from './dto';
const jwt = require('jsonwebtoken');
import { SECRET } from '../../configs/config';
import { UserRO } from './user.interface';
import { validate } from 'class-validator';
import { HttpStatus, HttpException } from '@nestjs/common';
import * as crypto from 'crypto';
import {Role} from "nest-access-control";

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,
    @InjectRepository(RoleEntity)
    private readonly roleRepository: Repository<RoleEntity>
  ) {}

  async findAll(): Promise<UserRO[]> {
    const users = await this.userRepository.createQueryBuilder('user').leftJoinAndSelect('user.roles', 'role').getMany();
    return users.map( user => this.buildUserRO(user))
  }

  async findOne(loginUserDto: LoginUserDto): Promise<UserEntity> {
    const findOneOptions = {
      email: loginUserDto.email,
      password: crypto.createHmac('sha256', loginUserDto.password).digest('hex'),
    };
    return await this.userRepository.createQueryBuilder('user').where(findOneOptions).leftJoinAndSelect('user.roles', 'role').getOne();
  }

  async create(dto: CreateUserDto): Promise<UserRO> {

    // check uniqueness of username/email
    const {email, password} = dto;
    const qb = await getRepository(UserEntity)
      .createQueryBuilder('user')
      .where('user.email = :email', { email });

    const user = await qb.getOne();

    if (user) {
      const errors = {email: 'Email must be unique.'};
      throw new HttpException({message: 'Input data validation failed', errors}, HttpStatus.BAD_REQUEST);

    }

    // create new user
    let newUser = new UserEntity();
    newUser.email = email;
    newUser.password = password;

    // default is a regular user
    let role = new RoleEntity();
    role.role = 2;
    newUser.roles = [role];

    const errors = await validate(newUser);
    if (errors.length > 0) {
      const _errors = {email: 'Userinput is not valid.'};
      throw new HttpException({message: 'Input data validation failed', _errors}, HttpStatus.BAD_REQUEST);

    } else {

      await this.roleRepository.save(role);
      let savedUser = await this.userRepository.save(newUser);

      return this.buildUserRO(savedUser);
    }

  }

  async update(dto: UpdateUserDto): Promise<UserEntity> {
    let toUpdate = await this.userRepository.findOne(dto.id);
    delete toUpdate.password;

    let updated = Object.assign(toUpdate, dto);
    return await this.userRepository.save(updated);
  }

  async delete(email: string): Promise<any> {
    return await this.userRepository.delete({ email: email});
  }

  async findById(id: number): Promise<UserRO> {
    const user = await this.userRepository.createQueryBuilder('user').where({id: id}).leftJoinAndSelect('user.roles', 'role').getOne();

    if (!user) {
      const errors = {User: ' not found'};
      throw new HttpException({errors}, 412);
    }

    return this.buildUserRO(user);
  }

  async findByEmail(email: string): Promise<UserRO> {
    const user = await this.userRepository.createQueryBuilder('user').where({email: email}).leftJoinAndSelect('user.roles', 'role').getOne();
    return this.buildUserRO(user);
  }

  public generateJWT(user) {
    let today = new Date();
    let exp = new Date(today);
    exp.setDate(today.getDate() + 60);

    return jwt.sign({
      id: user.id,
      email: user.email,
      exp: exp.getTime() / 1000,
    }, SECRET);
  }

  private buildUserRO(user: UserEntity) {
    const roles = user.roles.map( role => {
      return role.role;
    });
    const userRO = {
      id: user.id,
      email: user.email,
      bio: user.bio,
      token: this.generateJWT(user),
      loginCount: user.loginCount,
      roles: roles
    };

    return {user: userRO};
  }
}
