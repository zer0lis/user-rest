import {Entity, PrimaryGeneratedColumn, Column, BeforeInsert, JoinTable, ManyToMany} from 'typeorm';
import { IsEmail } from 'class-validator';
import * as crypto from 'crypto';

@Entity('user')
export class UserEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  @IsEmail()
  email: string;

  @Column({default: ''})
  bio: string;

  @Column()
  password: string;

  @Column({default: 0})
  loginCount: number;

  @ManyToMany(type => RoleEntity, role => role.users, { onDelete: 'CASCADE' })
  @JoinTable()
  roles: RoleEntity[];

  @BeforeInsert()
  hashPassword() {
    this.password = crypto.createHmac('sha256', this.password).digest('hex');
  }
}
// 1 - admin
// 2 - regular user
@Entity('role')
export class RoleEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column({default: 1})
  role: number;
  @ManyToMany(type => UserEntity, user => user.roles)
  users: UserEntity[];
}