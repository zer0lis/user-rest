import {MiddlewareConsumer, Module, NestModule, RequestMethod} from '@nestjs/common';
import {UserController} from './user.controller';
import {TypeOrmModule} from '@nestjs/typeorm';
import {RoleEntity, UserEntity} from './user.entity';
import {UserService} from './user.service';
import {AuthMiddleware, AuthorizationMiddleware} from './user.middleware';

@Module({
  imports: [TypeOrmModule.forFeature([UserEntity, RoleEntity])],
  providers: [UserService],
  controllers: [
    UserController
  ],
  exports: [UserService]
})
export class UserModule implements NestModule {
  public configure(consumer: MiddlewareConsumer) {
    consumer
        .apply(AuthMiddleware)
        .forRoutes(
            {path: 'user', method: RequestMethod.GET},
        )
        .apply(AuthorizationMiddleware)
        .exclude({path: 'users/login', method: RequestMethod.ALL})
        .forRoutes(
            {path: 'users', method: RequestMethod.GET},
            {path: 'users', method: RequestMethod.PUT},
            {path: 'user', method: RequestMethod.PUT},
            {path: 'user', method: RequestMethod.DELETE},
            {path: 'user', method: RequestMethod.POST}
        )
  }
}
